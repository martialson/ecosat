<?php

class acceuil_model extends CI_Model {

  function __construct() {
        parent::__construct();

        /*logged_in();
        $this->load->model('logo_model');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');*/
    }

    
      function get_logo(){
        $this->db->select('*');
        $this->db->from('logo');
        $this->db->order_by('logo_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

       function get_contact(){
        $this->db->select('*');
        $this->db->from('contact_info');
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function get_slides(){
        $this->db->select('*');
        $this->db->from('slides');
        $this->db->order_by('slide_id','desc');
        $this->db->limit(3);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function get_apropos_de_finasys(){
        $this->db->select('*');
        $this->db->from('apropos_de_finasys');
        $this->db->order_by('apropos_finasys_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function get_pourquoi_choisir_finasys(){
        $this->db->select('*');
        $this->db->from('pourquoi_choisir_finasys');
        $this->db->order_by('choisir_finasys_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function get_nos_services(){
        $this->db->select('*');
        $this->db->from('nos_services');
        $this->db->order_by('nos_services_id','desc');
        $this->db->limit(3);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_nos_competences(){
        $this->db->select('*');
        $this->db->from('nos_competences');
        $this->db->order_by('nos_competences_id','desc');
        $this->db->limit(5);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_temoignages(){
        $this->db->select('*');
        $this->db->from('temoignages');
        $this->db->order_by('temoignages_id','desc');
        $this->db->limit(3);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_partenaires(){
        $this->db->select('*');
        $this->db->from('partenaires');
        $this->db->order_by('partenaires_id','asc');
        $this->db->limit(10);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }



       function  ins_newsletter($mail){
        $this->db->insert('newsletter', $mail);
      }
      

}
