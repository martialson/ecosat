<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Acceuil extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('acceuil_model');
        $this->load->helper('active');
    }

    function index() {
    	$img_logo = array();
        $img_logo['free_logo'] = $this->acceuil_model->get_logo();
        $img_logo['contact'] = $this->acceuil_model->get_contact();
        $img_logo['slides'] = $this->acceuil_model->get_slides();
        $img_logo['apropos_de_finasys'] = $this->acceuil_model->get_apropos_de_finasys();
        $img_logo['pourquoi_choisir_finasys'] = $this->acceuil_model->get_pourquoi_choisir_finasys();
        $img_logo['nos_services'] = $this->acceuil_model->get_nos_services();
        $img_logo['nos_competences'] = $this->acceuil_model->get_nos_competences();
        $img_logo['temoignages'] = $this->acceuil_model->get_temoignages();
    	$img_logo['partenaires'] = $this->acceuil_model->get_partenaires();
    	$this->load->view('acceuil_view',$img_logo);
    }

    function newsletter() {
        $this->form_validation->set_rules('le_mail', 'le_mail');
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('acceuil_view');
        }
        else
        {
        $data = array(
            'newsletter_id'               => '',
            'newsletter_email'            => $this->input->post('le_mail'),
            'newsletter_date'             => date('Y-m-d'),
            'newsletter_ref'              => date('His').rand()
        );
        $this->acceuil_model->ins_newsletter($data);
        redirect();
       }
    }

}
