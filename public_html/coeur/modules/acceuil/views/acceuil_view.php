<?php $this->load->view('front/header.php'); ?>
<body>
<div class="page">
  <!--========================================================
                            HEADER
  =========================================================-->
  <header class="vide" data-vide-bg="video/video-bg">

    <div class="container vide_content">
      <div class="brand">
        <img src="<?php echo base_url(); ?>assets/images/logo_ecosat.png" style="max-width:200px;" alt=""/>


        <h1 class="brand_name">
          <a href="<?php echo base_url(); ?>">
            EcoSat
          </a>
        </h1>
        <p class="brand_slogan" style="color: #EA973E;">
          L&#39;internet par sat&eacute;lite
        </p>


<nav>
  <ul>
    <li>
      <a href="<?php echo base_url(); ?>">acceuil</a>
    </li>
    <li>
      <a href="<?php echo base_url(); ?>nos_services">Nos Services</a>
    </li>
    <li>
      <a href="<?php echo base_url(); ?>nos_offres">Nos Offres</a>
    </li>
    <li>
      <a href="<?php echo base_url(); ?>contact">Contact</a>
    </li>
  </ul>
</nav>


      </div>

      <!--h2>performance </h2-->

      <h3>Accessible en tout lieu et en tout temps</h3>

      <div class="card holdingpack">
        <img class="card-img-top" src="..." alt="Card image cap" style="display: none;">
        <div class="card-body">
          <h5 class="card-title">HOME</h5>
          <p class="card-text">1 Mois</p>
          
          <div class="card-text mypack">
          <div class="myunderpack"><div style="margin-top: 16px;"></div>5GB</div>
          <div style="display: inline-block;width:68%;font: 900 18px tahoma;">9 000 FCFA<br>
          5 Mbps download<br>
          3 Mbps upload
          </div>
          </div>

          <div class="card-text mypack">
          <div class="myunderpack"><div style="margin-top: 16px;"></div>10GB</div>
          <div style="display: inline-block;width:68%;font: 900 18px tahoma;">15 000 FCFA<br>
          5 Mbps download<br>
          3 Mbps upload
          </div>
          </div>

          <div class="card-text mypack">
          <div class="myunderpack"><div style="margin-top: 16px;"></div>20GB</div>
          <div style="display: inline-block;width:68%;font: 900 18px tahoma;">26 000 FCFA<br>
          5 Mbps download<br>
          3 Mbps upload
          </div>
          </div>

          <div class="card-text mypack">
          <div class="myunderpack"><div style="margin-top: 16px;"></div>30GB</div>
          <div style="display: inline-block;width:68%;font: 900 18px tahoma;">35 000 FCFA<br>
          10 Mbps download<br>
          3 Mbps upload
          </div>
          </div>

          <a href="#" class="btn btn-primary" style="margin: 0px;padding:10px;border-radius:4px;height:17px;font: 300 16px tahoma;margin-bottom: 10px;">Voir plus</a>
        </div>
      </div>


      <div class="card holdingpack">
        <img class="card-img-top" src="..." alt="Card image cap" style="display: none;">
        <div class="card-body">
          <h5 class="card-title">PRO</h5>
          <p class="card-text">1 Mois</p>
          
          <div class="card-text mypack">
          <div class="myunderpack"><div style="margin-top: 16px;"></div>50GB</div>
          <div style="display: inline-block;width:68%;font: 900 18px tahoma;">58 000 FCFA<br>
          20 Mbps download<br>
          3 Mbps upload
          </div>
          </div>

          <div class="card-text mypack">
          <div class="myunderpack"><div style="margin-top: 16px;"></div>100GB</div>
          <div style="display: inline-block;width:68%;font: 900 18px tahoma;">15 000 FCFA<br>
          20 Mbps download<br>
          3 Mbps upload
          </div>
          </div>

          <div class="card-text mypack">
          <div class="myunderpack"><div style="margin-top: 16px;"></div>300GB</div>
          <div style="display: inline-block;width:68%;font: 900 18px tahoma;">26 000 FCFA<br>
          20 Mbps download<br>
          3 Mbps upload
          </div>
          </div>

          <div class="card-text mypack">
          <div class="myunderpack"><div style="margin-top: 16px;"></div>500GB</div>
          <div style="display: inline-block;width:68%;font: 900 18px tahoma;">35 000 FCFA<br>
          20 Mbps download<br>
          3 Mbps upload
          </div>
          </div>

          <a href="#" class="btn btn-primary" style="margin: 0px;padding:10px;border-radius:4px;height:17px;font: 300 16px tahoma;margin-bottom: 10px;">Voir plus</a>
        </div>
      </div>

    </div>
  </header>
  <!--========================================================
                            CONTENT
  =========================================================-->
  <main>
    <section class="well">
      <div class="container center">
        <h2>
          L&#39;Internet haut d&eacute;bit, <br/>
          Partout et &agrave; moindre co&ucirc;t
        </h2>
        <hr/>
        <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et <br/>
           dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
           commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
           nulla pariatur.
        </p>

        <div class="row">
          <div class="grid_4">
            <div class="lazy-img" style="padding-bottom:70.27027027027027%;">
              <img data-src="<?php echo base_url(); ?>assets/images/page-1_img01.jpg" src="#" alt=""/>
            </div>
            <h3 class="primary">Sam Kromstain</h3>

            <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
               etdolore magna aliqua. </p>
          </div>
          <div class="grid_4">
            <div class="lazy-img" style="padding-bottom:70.27027027027027%;">
              <img data-src="<?php echo base_url(); ?>assets/images/page-1_img02.jpg" src="#" alt=""/>
            </div>
            <h3 class="primary">Alan Smith</h3>

            <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
               etdolore magna aliqua. </p>
          </div>
          <div class="grid_4">
            <div class="lazy-img" style="padding-bottom:70.27027027027027%;">
              <img data-src="<?php echo base_url(); ?>assets/images/page-1_img03.jpg" src="#" alt=""/>
            </div>
            <h3 class="primary">John Franklin </h3>

            <p>Lorem ipsum dolor sit amet conse<?php echo base_url(); ?>assets/ ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
               etdolore magna aliqua. </p>
          </div>
        </div>
      </div>
    </section>

    <section class="parallax center bg-secondary2" data-url="<?php echo base_url(); ?>assets/images/parallax.jpg" data-mobile="true" data-speed="0.5">
      <div class="well3">
        <div class="container">
          <h2>
            Best Joomla templates, WordPress themes <br/>
            and free Joomla modules
          </h2>
          <hr/>
          <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
            <br/>
             dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
             ea
             commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
             nulla pariatur.
          </p>
        </div>
      </div>
    </section>

    <section class="well well__ins1 center">
      <div class="container">
        <div class="row">
          <div class="grid_4">
            <div class="flaticon-toilets1"></div>
            <hr/>
            <h3>Rapidit&eacute;</h3>

            <p>Lorem ipsum dolor sit amet conse ctetur <br/> adipisicing elit, sed do eiusmod tempor incididunt ut
               labore
               et </p>
          </div>
          <div class="grid_4">
            <div class="flaticon-coffee69"></div>
            <hr/>
            <h3>Simplicit&eacute; d&#39;utilisation</h3>

            <p>Lorem ipsum dolor sit amet conse ctetur <br/> adipisicing elit, sed do eiusmod tempor incididunt ut
               labore
               et </p>
          </div>
          <div class="grid_4">
            <div class="flaticon-hotel70"></div>
            <hr/>
            <h3>Un Prix abordable</h3>

            <p>Lorem ipsum dolor sit amet conse ctetur <br/> adipisicing elit, sed do eiusmod tempor incididunt ut
               labore
               et </p>
          </div>
        </div>
      </div>
    </section>


    <section class="well well__ins1 center">
      <div class="container">
        <div class="row">
          <div class="grid_4">
            <div class="flaticon-toilets1"></div>
            <hr/>
            <h3>Fiabilit&eacute;</h3>

            <p>Lorem ipsum dolor sit amet conse ctetur <br/> adipisicing elit, sed do eiusmod tempor incididunt ut
               labore
               et </p>
          </div>
          <div class="grid_4">
            <div class="flaticon-coffee69"></div>
            <hr/>
            <h3>Accessible partout</h3>

            <p>Lorem ipsum dolor sit amet conse ctetur <br/> adipisicing elit, sed do eiusmod tempor incididunt ut
               labore
               et </p>
          </div>
          <div class="grid_4">
            <div class="flaticon-hotel70"></div>
            <hr/>
            <h3></h3>

            <p>Lorem ipsum dolor sit amet conse ctetur <br/> adipisicing elit, sed do eiusmod tempor incididunt ut
               labore
               et </p>
          </div>
        </div>
      </div>
    </section>



    <section class="well">
      <div class="container center">
        <h2>Nos Partenaires</h2>
        <hr/>
        <div class="owl-carousel">
          <div class="item">
            <blockquote>
              <img src="<?php echo base_url(); ?>assets/images/page-1_img13.jpg" alt=""/>

              <p><q>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                    et <br/>
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum
                    dolore eu fugiat nulla pariatur.</q></p>

              <h3 class="primary"><cite>Michael Freeman</cite></h3>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <img src="<?php echo base_url(); ?>assets/images/page-1_img09.jpg" alt=""/>

              <p><q>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                    et <br/>
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum
                    dolore eu fugiat nulla pariatur.</q></p>

              <h3 class="primary"><cite>sed do eiusmod</cite></h3>
            </blockquote>
          </div>
        </div>
      </div>

      <div class="container center">
        <ul class="flex-list">
          <li><img src="<?php echo base_url(); ?>assets/images/page-1_img10.jpg" alt=""/></li>
          <li><img src="<?php echo base_url(); ?>assets/images/page-1_img11.jpg" alt=""/></li>
          <li><img src="<?php echo base_url(); ?>assets/images/page-1_img12.jpg" alt=""/></li>
        </ul>
      </div>
    </section>

    <section class="well2 bg-secondary">
      <div class="container center">
        <div class="row">
          <div class="grid_8 preffix_2">
            <h2>Newsletter</h2>

            <form id="subscribe-form" class="subscribe-form">

              <label class="email">
                <input type="email" value="E - mail">
                <span class="error">*Invalid email.</span>
                <span class="success">Your subscription request has been sent!</span>
              </label>
              <a class="btn" data-type="submit" href="#">S'&#39;inscrire</a>
            </form>
          </div>
        </div>
      </div>
    </section>
  </main>
<?php $this->load->view('front/footer.php'); ?>