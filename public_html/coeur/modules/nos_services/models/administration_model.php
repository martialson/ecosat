<?php

class Administration_model extends CI_Model {


    function connect($mail,$pass){
        $this->db->select('*');
        $this->db->from('utilisateur');
        $this->db->where('utilisateur_email',$mail);      
        $this->db->where('utilisateur_password',md5($pass));      
        $result = $this->db->get();
        return $result->result();
      }



/////////////////////////////////////ACCEUIL////////////////////////////////////////////////////////////

   


    //////////////////////////////////////////////////////////////////////////////////////////////

    function ins_apropos_de_finasys($donnee){
        $this->db->insert('apropos_de_finasys', $donnee);
    }

     function ins_choisir_finasys($donnee){
        $this->db->insert('pourquoi_choisir_finasys', $donnee);
    }

     function ins_nos_services($donnee){
        $this->db->insert('nos_services', $donnee);
    }

     function ins_nos_competences($donnee){
        $this->db->insert('nos_competences', $donnee);
    }

    function ins_temoignages($donnee){
        $this->db->insert('temoignages', $donnee);
    }

    function ins_partenaires($donnee){
        $this->db->insert('partenaires', $donnee);
    }


///////////////////////////////

    function update_apropos_finasys($donnee){
        $this->db->update('apropos_de_finasys', $donnee);
    }

    function update_pourquoi_choisir_finasys($donnee){
        $this->db->update('pourquoi_choisir_finasys', $donnee);
    }


    function update_nos_services($id, $donnee){
        $this->db->where('nos_services_id', $id);
        $this->db->update('nos_services', $donnee);
    }


    function update_nos_competences($donnee){
        $this->db->update('nos_competences', $donnee);
    }

    function update_temoignages($id, $donnee){
        $this->db->where('temoignages_id', $id);
        $this->db->update('temoignages', $donnee);
    }

    function update_partenaires($id, $donnee){
        $this->db->where('partenaires_id', $id);
        $this->db->update('partenaires', $donnee);
    }
//////////////////////////////////////
    
    function apropos_de_finasys(){
        $this->db->select('*');
        $this->db->from('apropos_de_finasys');
        $this->db->order_by('apropos_finasys_id','DESC');           
        $this->db->limit(1);        
        $result = $this->db->get();
        return $result->result();
    }

    function pourquoi_choisir_finasys(){
        $this->db->select('*');
        $this->db->from('pourquoi_choisir_finasys');      
        $this->db->order_by('choisir_finasys_id','DESC');           
        $this->db->limit(1);        
        $result = $this->db->get();
        return $result->result();
    }

    function nos_services(){
        $this->db->select('*');
        $this->db->from('nos_services');
        $this->db->order_by('nos_services_id','DESC');           
        $this->db->limit(3);        
        $result = $this->db->get();
        return $result->result();
    }

    function nos_competences(){
        $this->db->select('*');
        $this->db->from('nos_competences');         
        $this->db->order_by('nos_competences_id','DESC');           
        $this->db->limit(5);        
        $result = $this->db->get();
        return $result->result();
    }

    function quelques_temoignages(){
        $this->db->select('*');
        $this->db->from('temoignages');          
        $this->db->order_by('temoignages_id','DESC');           
        $this->db->limit(3);        
        $result = $this->db->get();
        return $result->result();
    }
   
    function nos_partenaires(){
        $this->db->select('*');
        $this->db->from('partenaires');       
        $this->db->order_by('partenaires_id','DESC');           
        $this->db->limit(5);        
        $result = $this->db->get();
        return $result->result();
    }
////////////////////////////////////////////
    
    
      function recup_apropos_de_finasys() 
      {
        $this->db->select('*');
        $this->db->from('apropos_de_finasys');
        $this->db->order_by('apropos_finasys_id','DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function recup_pourquoi_choisir_finasys() 
      {
        $this->db->select('*');
        $this->db->from('pourquoi_choisir_finasys');
        $this->db->order_by('choisir_finasys_id','DESC');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function recup_nos_services($id) 
      {
        $this->db->select('*');
        $this->db->from('nos_services');
        $this->db->where('nos_services_id',$id);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function recup_nos_competences() 
      {
        $this->db->select('*');
        $this->db->from('nos_competences');
        $this->db->order_by('nos_competences_id','DESC');
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function recup_temoignages($id) 
      {
        $this->db->select('*');
        $this->db->from('temoignages');
        $this->db->where('temoignages_id',$id);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function recup_partenaires($id) 
      {
        $this->db->select('*');
        $this->db->from('partenaires');
        $this->db->where('partenaires_id',$id);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////A PROPOS////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

function historique(){
        $this->db->select('*');
        $this->db->from('historique');
        $this->db->order_by('historique_id','DESC');           
        $this->db->limit(1);        
        $result = $this->db->get();
        return $result->result();
    }

    function objectifs(){
        $this->db->select('*');
        $this->db->from('objectifs');
        $this->db->order_by('objectifs_id','DESC');           
        $this->db->limit(1);        
        $result = $this->db->get();
        return $result->result();
    }

    function vision(){
        $this->db->select('*');
        $this->db->from('vision');
        $this->db->order_by('vision_id','DESC');           
        $this->db->limit(1);        
        $result = $this->db->get();
        return $result->result();
    }

    function missions(){
        $this->db->select('*');
        $this->db->from('missions');
        $this->db->order_by('missions_id','DESC');           
        $this->db->limit(1);        
        $result = $this->db->get();
        return $result->result();
    }

    function notre_equipe(){
        $this->db->select('*');
        $this->db->from('notre_equipe');
        $this->db->order_by('notre_equipe_id','DESC');           
        $this->db->limit(10);        
        $result = $this->db->get();
        return $result->result();
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////


    function ins_historique($donnee){
        $this->db->insert('historique', $donnee);
    }

    function ins_objectifs($donnee){
        $this->db->insert('objectifs', $donnee);
    }

    function ins_vision($donnee){
        $this->db->insert('vision', $donnee);
    }

    function ins_missions($donnee){
        $this->db->insert('missions', $donnee);
    }

    function ins_notre_equipe($donnee){
        $this->db->insert('notre_equipe', $donnee);
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////


    function recup_historique() 
      {
        $this->db->select('*');
        $this->db->from('historique');
        $this->db->order_by('historique_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function recup_objectifs() 
      {
        $this->db->select('*');
        $this->db->from('objectifs');
        $this->db->order_by('objectifs_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function recup_missions() 
      {
        $this->db->select('*');
        $this->db->from('missions');
        $this->db->order_by('missions_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

    function recup_vision() 
      {
        $this->db->select('*');
        $this->db->from('vision');
        $this->db->order_by('vision_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function recup_notre_equipe($id) 
      {
        $this->db->select('*');
        $this->db->from('notre_equipe');
        $this->db->where('notre_equipe_id', $id);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }


///////////////////////////////////////////////////////////////////////////////////////////////////////

    function update_historique($donnee){
        $this->db->update('historique', $donnee);
    }  

    function update_objectifs($donnee){
        $this->db->update('objectifs', $donnee);
    }

    function update_missions($donnee){
        $this->db->update('missions', $donnee);
    }

    function update_vision($donnee){
        $this->db->update('vision', $donnee);
    }

    function update_notre_equipe($id, $donnee){
        $this->db->where('notre_equipe_id', $id);
        $this->db->update('notre_equipe', $donnee);
    }



//////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////NOS SERVICES////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////


    function logiciels_standards_de_gestion(){
        $this->db->select('*');
        $this->db->from('logiciels_standards_de_gestion');
        $this->db->order_by('logiciels_standards_de_gestion_id','DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result();
    }

    function logiciels_verticaux(){
        $this->db->select('*');
        $this->db->from('logiciels_verticaux');
        $this->db->order_by('logiciels_verticaux_id','DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result();
    }

    function developpement_specifique_et_sur_mesure(){
        $this->db->select('*');
        $this->db->from('developpement_specifique_et_sur_mesure');
        $this->db->order_by('developpement_specifique_et_sur_mesure_id','DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result();
    }

    function integration_ERP(){
        $this->db->select('*');
        $this->db->from('integration_ERP');
        $this->db->order_by('integration_ERP_id','DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result();
    }

    function nos_references(){
        $this->db->select('*');
        $this->db->from('nos_references');
        $this->db->order_by('nos_references_id','DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result();
    }

    function infogerance_et_assistance_maintenance(){
        $this->db->select('*');
        $this->db->from('infogerance_et_assistance_maintenance');
        $this->db->order_by('infogerance_et_assistance_maintenance_id','DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result();
    }

    function securite_des_reseaux_et_protection_des_donnee(){
        $this->db->select('*');
        $this->db->from('securite_des_reseaux_et_protection_des_donnee');
        $this->db->order_by('securite_des_reseaux_et_protection_des_donnee_id','DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result();
    }

    function infrastructures_serveurs(){
        $this->db->select('*');
        $this->db->from('infrastructures_serveurs');
        $this->db->order_by('infrastructures_serveurs_id','DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result();
    }

    function nos_referencesIT(){
        $this->db->select('*');
        $this->db->from('nos_referencesIT');
        $this->db->order_by('nos_referencesIT_id','DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result();
    }

    function synchronisation_de_fichier(){
        $this->db->select('*');
        $this->db->from('synchronisation_de_fichier');
        $this->db->order_by('synchronisation_de_fichier_id','DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result();
    }

    function hebergement_de_serveurs_virtuels(){
        $this->db->select('*');
        $this->db->from('hebergement_de_serveurs_virtuels');
        $this->db->order_by('hebergement_de_serveurs_virtuels_id','DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result();
    }

    function sauvegarde_des_donnee_deportee_deportee(){
        $this->db->select('*');
        $this->db->from('sauvegarde_des_donnee_deportee_deportee');
        $this->db->order_by('sauvegarde_des_donnee_deportee_deportee_id','DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result();
    }

    function solution_de_messagerie_collaborative(){
        $this->db->select('*');
        $this->db->from('solution_de_messagerie_collaborative');
        $this->db->order_by('solution_de_messagerie_collaborative_id','DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result();
    }
/////////////////////////////////////////////////////////////////////////////////////////////////

    function ins_logiciels_standards_de_gestion($donnee){
        $this->db->insert('logiciels_standards_de_gestion', $donnee);
    }

    function ins_logiciels_verticaux($donnee){
        $this->db->insert('logiciels_verticaux', $donnee);
    }

    function ins_developpement_specifique_et_sur_mesure($donnee){
        $this->db->insert('developpement_specifique_et_sur_mesure', $donnee);
    }

    function ins_integration_ERP($donnee){
        $this->db->insert('integration_ERP', $donnee);
    }

    function ins_nos_references($donnee){
        $this->db->insert('nos_references', $donnee);
    }

    function ins_infogerance_et_assistance_maintenance($donnee){
        $this->db->insert('infogerance_et_assistance_maintenance', $donnee);
    }

    function ins_securite_des_reseaux_et_protection_des_donnee($donnee){
        $this->db->insert('securite_des_reseaux_et_protection_des_donnee', $donnee);
    }

    function ins_infrastructures_serveurs($donnee){
        $this->db->insert('infrastructures_serveurs', $donnee);
    }

    function ins_nos_referencesIT($donnee){
        $this->db->insert('nos_referencesIT', $donnee);
    }

    function ins_synchronisation_de_fichier($donnee){
        $this->db->insert('synchronisation_de_fichier', $donnee);
    }

    function ins_hebergement_de_serveurs_virtuels($donnee){
        $this->db->insert('hebergement_de_serveurs_virtuels', $donnee);
    }

    function ins_sauvegarde_des_donnee_deportee_deportee($donnee){
        $this->db->insert('sauvegarde_des_donnee_deportee_deportee', $donnee);
    }

    function ins_solution_de_messagerie_collaborative($donnee){
        $this->db->insert('solution_de_messagerie_collaborative', $donnee);
    }

//////////////////////////////////////////////////////////////////////////////////////


    function recup_logiciels_standards_de_gestion() 
      {
        $this->db->select('*');
        $this->db->from('logiciels_standards_de_gestion');
        $this->db->order_by('logiciels_standards_de_gestion_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }


      function recup_logiciels_verticaux() 
      {
        $this->db->select('*');
        $this->db->from('logiciels_verticaux');
        $this->db->order_by('logiciels_verticaux_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function recup_developpement_specifique_et_sur_mesure() 
      {
        $this->db->select('*');
        $this->db->from('developpement_specifique_et_sur_mesure');
        $this->db->order_by('developpement_specifique_et_sur_mesure_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function recup_integration_ERP() 
      {
        $this->db->select('*');
        $this->db->from('integration_ERP');
        $this->db->order_by('integration_ERP_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }


      function recup_nos_references() 
      {
        $this->db->select('*');
        $this->db->from('nos_references');
        $this->db->order_by('nos_references_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }


      function recup_infogerance_et_assistance_maintenance() 
      {
        $this->db->select('*');
        $this->db->from('infogerance_et_assistance_maintenance');
        $this->db->order_by('infogerance_et_assistance_maintenance_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }
      
       function recup_securite_des_reseaux_et_protection_des_donnee() 
      {
        $this->db->select('*');
        $this->db->from('infogerance_et_assistance_maintenance');
        $this->db->order_by('infogerance_et_assistance_maintenance_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }


      function recup_infrastructures_serveurs() 
      {
        $this->db->select('*');
        $this->db->from('infrastructures_serveurs');
        $this->db->order_by('infrastructures_serveurs_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }
      
       function recup_nos_referencesIT() 
      {
        $this->db->select('*');
        $this->db->from('nos_referencesIT');
        $this->db->order_by('nos_referencesIT_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function recup_synchronisation_de_fichier() 
      {
        $this->db->select('*');
        $this->db->from('synchronisation_de_fichier');
        $this->db->order_by('synchronisation_de_fichier_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function recup_hebergement_de_serveurs_virtuels() 
      {
        $this->db->select('*');
        $this->db->from('hebergement_de_serveurs_virtuels');
        $this->db->order_by('hebergement_de_serveurs_virtuels_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function recup_sauvegarde_des_donnee_deportee_deportee() 
      {
        $this->db->select('*');
        $this->db->from('sauvegarde_des_donnee_deportee_deportee');
        $this->db->order_by('sauvegarde_des_donnee_deportee_deportee_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function recup_solution_de_messagerie_collaborative() 
      {
        $this->db->select('*');
        $this->db->from('solution_de_messagerie_collaborative');
        $this->db->order_by('solution_de_messagerie_collaborative_id','DESC');           
        $this->db->limit(1);        
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }


//////////////////////////////////////////////////////////////////////////////////////////

    function update_logiciels_standards_de_gestion($donnee){
        $this->db->update('logiciels_standards_de_gestion', $donnee);
    }

    function update_logiciels_verticaux($donnee){
        $this->db->update('logiciels_verticaux', $donnee);
    }

    function update_developpement_specifique_et_sur_mesure($donnee){
        $this->db->update('developpement_specifique_et_sur_mesure', $donnee);
    }

    function update_integration_ERP($donnee){
        $this->db->update('integration_ERP', $donnee);
    }

    function update_nos_references($donnee){
        $this->db->update('nos_references', $donnee);
    }

    function update_infogerance_et_assistance_maintenance($donnee){
        $this->db->update('infogerance_et_assistance_maintenance', $donnee);
    }

    function update_securite_des_reseaux_et_protection_des_donnee($donnee){
        $this->db->update('securite_des_reseaux_et_protection_des_donnee', $donnee);
    }

    function update_infrastructures_serveurs($donnee){
        $this->db->update('infrastructures_serveurs', $donnee);
    }

    function update_nos_referencesIT($donnee){
        $this->db->update('nos_referencesIT', $donnee);
    }

    function update_synchronisation_de_fichier($donnee){
        $this->db->update('synchronisation_de_fichier', $donnee);
    }

    function update_hebergement_de_serveurs_virtuels($donnee){
        $this->db->update('hebergement_de_serveurs_virtuels', $donnee);
    }

    function update_sauvegarde_des_donnee_deportee_deportee($donnee){
        $this->db->update('sauvegarde_des_donnee_deportee_deportee', $donnee);
    }

    function update_solution_de_messagerie_collaborative($donnee){
        $this->db->update('solution_de_messagerie_collaborative', $donnee);
    }





//////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////SLIDES////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

     function slides() {
        $this->db->select('*');
        $this->db->from('slides');
        $this->db->order_by('slide_id','desc');           
        $this->db->limit(3);
        $result = $this->db->get();
        return $result->result();
    }

    function slides_uniq($id) {
        $this->db->select('*');
        $this->db->from('slides');
        $this->db->where('slide_id',$id);           
        $result = $this->db->get();
        return $result->result();
    }

    function ins_slides($donnee){
        $this->db->insert('slides', $donnee);
    }

/////////////////////////////////////////////////////////////////
    function update_slides($id, $donnee){
        $this->db->where('slide_id', $id);
        $this->db->update('slides', $donnee);
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////CONTACT////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

    function contact_list() {
        $this->db->select('*');
        $this->db->from('contact');
        $this->db->order_by('contact_id','DESC');           
        $this->db->limit(100);        
        $result = $this->db->get();
        return $result->result();
    }

     function contact_info_list() {
        $this->db->select('*');
        $this->db->from('contact_info');
        $this->db->order_by('contact_id','DESC');           
        $this->db->limit(1);        
        $result = $this->db->get();
        return $result->result();
    }

    function recup_contact_info($id) 
      {
        $this->db->select('*');
        $this->db->from('contact_info');
        $this->db->where('contact_id', $id);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function recup_contact_msg($id) 
      {
        $this->db->select('*');
        $this->db->from('contact');
        $this->db->where('contact_id', $id);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }


      function update_contact_info($id, $donnee){
        $this->db->where('contact_id', $id);
        $this->db->update('contact_info', $donnee);
    }


    function ins_contact_info($donnee){
        $this->db->insert('contact_info', $donnee);
    }


    function ins_contact_reponses($donnee){
        $this->db->insert('reponses_contact', $donnee);
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////NEWSLETTER////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

     function newsletter() {
        $this->db->select('*');
        $this->db->from('newsletter');
        $this->db->order_by('newsletter_id','DESC');           
        $this->db->limit(100);        
        $result = $this->db->get();
        return $result->result();
    }

    function recup_newsletter($id) 
      {
        $this->db->select('*');
        $this->db->from('newsletter');
        $this->db->where('newsletter_id', $id);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

    function update_newsletter($id, $donnee){
        $this->db->where('newsletter_id', $id);
        $this->db->update('newsletter', $donnee);
    }


    function ins_newsletter($donnee){
        $this->db->insert('newsletter', $donnee);
    }
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////PARAMETRES////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////


function parametres() {
        /*$this->db->select('*');
        $this->db->from('slides');
        $this->db->order_by('slide_id','DESC');           
        $this->db->limit(5);        
        $result = $this->db->get();
        return $result->result();*/
    }



//////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////SUPPRIMER////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////


    function del_apropos_de_finasys($id) {
      $this->db->delete('apropos_de_finasys', array('apropos_finasys_id' => $id));
    }

    function del_pourquoi_choisir_finasys($id) {
      $this->db->delete('pourquoi_choisir_finasys', array('choisir_finasys_id' => $id));
    }

    function del_nos_services($id) {
      $this->db->delete('nos_services', array('nos_services_id' => $id));
    }

    function del_temoignages($id) {
      $this->db->delete('temoignages', array('temoignages_id' => $id));
    }



////////////////////////////////////////////////////////////////////////////

    function del_historique($id) {
      $this->db->delete('historique', array('historique_id' => $id));
    }

    function del_objectifs($id) {
      $this->db->delete('objectifs', array('objectifs_id' => $id));
    }

    function del_vision($id) {
      $this->db->delete('vision', array('vision_id' => $id));
    }

    function del_missions($id) {
      $this->db->delete('missions', array('missions_id' => $id));
    }

    function del_notre_equipe($id) {
      $this->db->delete('notre_equipe', array('notre_equipe_id' => $id));
    }



//////////////////////////////////////////////////////////////////////



    function del_logiciels_standards_de_gestion($id) {
      $this->db->delete('logiciels_standards_de_gestion', array('logiciels_standards_de_gestion_id' => $id));
    }

    function del_logiciels_verticaux($id) {
      $this->db->delete('logiciels_verticaux', array('logiciels_verticaux_id' => $id));
    }

    function del_developpement_specifique_et_sur_mesure($id) {
      $this->db->delete('developpement_specifique_et_sur_mesure', array('developpement_specifique_et_sur_mesure_id' => $id));
    }

    function del_integration_ERP($id) {
      $this->db->delete('integration_ERP', array('integration_ERP_id' => $id));
    }

    function del_nos_references($id) {
      $this->db->delete('nos_references', array('nos_references_id' => $id));
    }

    function del_infogerance_et_assistance_maintenance($id) {
      $this->db->delete('infogerance_et_assistance_maintenance', array('infogerance_et_assistance_maintenance_id' => $id));
    }

    function del_securite_des_reseaux_et_protection_des_donnee($id) {
      $this->db->delete('securite_des_reseaux_et_protection_des_donnee', array('securite_des_reseaux_et_protection_des_donnee_id' => $id));
    }

    function del_infrastructures_serveurs($id) {
      $this->db->delete('infrastructures_serveurs', array('infrastructures_serveurs_id' => $id));
    }

    function del_nos_referencesIT($id) {
      $this->db->delete('nos_referencesIT', array('nos_referencesIT_id' => $id));
    }

    function del_synchronisation_de_fichier($id) {
      $this->db->delete('synchronisation_de_fichier', array('synchronisation_de_fichier_id' => $id));
    }

    function del_hebergement_de_serveurs_virtuels($id) {
      $this->db->delete('hebergement_de_serveurs_virtuels', array('hebergement_de_serveurs_virtuels_id' => $id));
    }

    function del_sauvegarde_des_donnee_deportee_deportee($id) {
      $this->db->delete('sauvegarde_des_donnee_deportee_deportee', array('sauvegarde_des_donnee_deportee_deportee_id' => $id));
    }

    function del_solution_de_messagerie_collaborative($id) {
      $this->db->delete('solution_de_messagerie_collaborative', array('solution_de_messagerie_collaborative_id' => $id));
    }





    function del_slides($id) {
      $this->db->delete('slides', array('slide_id' => $id));
    }

    function del_partenaires($id) {
      $this->db->delete('partenaires', array('partenaires_id' => $id));
    }

    function del_newsletter($id) {
      $this->db->delete('newsletter', array('newsletter_id' => $id));
    }

    function del_contact_info($id) {
      $this->db->delete('contact_info', array('contact_id' => $id));
    }

    function del_contact($id) {
      $this->db->delete('contact', array('contact_id' => $id));
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



}
