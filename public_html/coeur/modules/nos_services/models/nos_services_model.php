<?php

class Nos_services_model extends CI_Model {


       function  get_logo(){
        $this->db->select('*');
        $this->db->from('logo');
        $this->db->order_by('logo_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function get_contact(){
        $this->db->select('*');
        $this->db->from('contact_info');
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }


      function  get_logiciels_standards_de_gestion(){
        $this->db->select('*');
        $this->db->from('logiciels_standards_de_gestion');
        $this->db->order_by('logiciels_standards_de_gestion_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }


      function  get_logiciels_verticaux(){
        $this->db->select('*');
        $this->db->from('logiciels_verticaux');
        $this->db->order_by('logiciels_verticaux_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }


      function  get_developpement_specifique_et_sur_mesure(){
        $this->db->select('*');
        $this->db->from('developpement_specifique_et_sur_mesure');
        $this->db->order_by('developpement_specifique_et_sur_mesure_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_integration_ERP(){
        $this->db->select('*');
        $this->db->from('integration_erp');
        $this->db->order_by('integration_ERP_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_Nos_references(){
        $this->db->select('*');
        $this->db->from('Nos_references');
        $this->db->order_by('Nos_references_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }


      function  get_infogerance_et_assistance_maintenance(){
        $this->db->select('*');
        $this->db->from('infogerance_et_assistance_maintenance');
        $this->db->order_by('infogerance_et_assistance_maintenance_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_securite_des_reseaux_et_protection_des_donnee(){
        $this->db->select('*');
        $this->db->from('securite_des_reseaux_et_protection_des_donnee');
        $this->db->order_by('securite_des_reseaux_et_protection_des_donnee_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_infrastructures_serveurs(){
        $this->db->select('*');
        $this->db->from('infrastructures_serveurs');
        $this->db->order_by('infrastructures_serveurs_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_nos_referencesIT(){
        $this->db->select('*');
        $this->db->from('nos_referencesIT');
        $this->db->order_by('nos_referencesIT_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_synchronisation_de_fichier(){
        $this->db->select('*');
        $this->db->from('synchronisation_de_fichier');
        $this->db->order_by('synchronisation_de_fichier_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_hebergement_de_serveurs_virtuels(){
        $this->db->select('*');
        $this->db->from('hebergement_de_serveurs_virtuels');
        $this->db->order_by('hebergement_de_serveurs_virtuels_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_sauvegarde_des_donnee_deportee_deportee(){
        $this->db->select('*');
        $this->db->from('sauvegarde_des_donnee_deportee_deportee');
        $this->db->order_by('sauvegarde_des_donnee_deportee_deportee_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_solution_de_messagerie_collaborative(){
        $this->db->select('*');
        $this->db->from('solution_de_messagerie_collaborative');
        $this->db->order_by('solution_de_messagerie_collaborative_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }






}
