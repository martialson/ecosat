<?php $this->load->view('front/header2.php', $var_menu_services); ?>

  <!--========================================================
                            CONTENT
  =========================================================-->
  <main>
    <section class="well">
      <div class="container center">
        <h2 style="color:#EA973E;">Nos services</h2>
        <h3>Partout et &agrave; moindre co&ucirc;t</h3>
        </h2>
        <hr/>
        <p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et <br/>
           dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
           commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
           nulla pariatur.
        </p>
      </div>
    </section>

    <section class="well well__ins1 center">
      <div class="container">
        <div class="row">
          <div class="grid_4">
            <div class="flaticon-toilets1"></div>
            <hr/>
            <h3>Rapidit&eacute;</h3>

            <p>Lorem ipsum dolor sit amet conse ctetur <br/> adipisicing elit, sed do eiusmod tempor incididunt ut
               labore
               et </p>
          </div>
          <div class="grid_3">
            <div class="flaticon-coffee69"></div>
            <hr/>
            <h3>Simplicit&eacute; d&#39;utilisation</h3>

            <p>Lorem ipsum dolor sit amet conse ctetur <br/> adipisicing elit, sed do eiusmod tempor incididunt ut
               labore
               et </p>
          </div>
          <div class="grid_4">
            <div class="flaticon-hotel70"></div>
            <hr/>
            <h3>Un Prix abordable</h3>

            <p>Lorem ipsum dolor sit amet conse ctetur <br/> adipisicing elit, sed do eiusmod tempor incididunt ut
               labore
               et </p>
          </div>
        </div>
      </div>
    </section>


    <section class="well well__ins1 center">
      <div class="container">
        <div class="row">
          <div class="grid_4">
            <div class="flaticon-toilets1"></div>
            <hr/>
            <h3>Fiabilit&eacute;</h3>

            <p>Lorem ipsum dolor sit amet conse ctetur <br/> adipisicing elit, sed do eiusmod tempor incididunt ut
               labore
               et </p>
          </div>
          <div class="grid_3">
            <div class="flaticon-coffee69"></div>
            <hr/>
            <h3>Accessible partout</h3>

            <p>Lorem ipsum dolor sit amet conse ctetur <br/> adipisicing elit, sed do eiusmod tempor incididunt ut
               labore
               et </p>
          </div>
          <div class="grid_4">
            <div class="flaticon-hotel70"></div>
            <hr/>
            <h3>Un Prix abordable</h3>

            <p>Lorem ipsum dolor sit amet conse ctetur <br/> adipisicing elit, sed do eiusmod tempor incididunt ut
               labore
               et </p>
          </div>
        </div>
      </div>
    </section>



    

    <section class="well2 bg-secondary">
      <div class="container center">
        <div class="row">
          <div class="grid_8 preffix_2">
            <h2>Newsletter</h2>

            <form id="subscribe-form" class="subscribe-form">

              <label class="email">
                <input type="email" value="E - mail">
                <span class="error">*Invalid email.</span>
                <span class="success">Your subscription request has been sent!</span>
              </label>
              <a class="btn" data-type="submit" href="#">S'&#39;inscrire</a>
            </form>
          </div>
        </div>
      </div>
    </section>
  </main>
<?php $this->load->view('front/footer.php'); ?>