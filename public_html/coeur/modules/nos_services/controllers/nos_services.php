<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Nos_services extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('active');
        $this->load->model('nos_services_model');
    }
    
    /*public function index() {
        $this->load->view('Developpementdelogiciels/contact_view');
    }*/

    public function index() {
        $var_user = array();
        $var_user['var_menu_services'] = "services";
        $this->load->view('nos_services_view', $var_user);
    }

   

}
