<?php

class Logo_model extends CI_Model {

    function getLogo(){
        $this->db->select('*');
        $this->db->from('logo');
        $this->db->order_by('logo_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function change_logo($donnee) {
      	//inlink(base_url().'assets/images/'.$image);
      	$this->db->insert('logo', $donnee);
      }

    
}
