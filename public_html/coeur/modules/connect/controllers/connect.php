<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Connect extends MX_Controller {

    function __construct() {
        parent::__construct();

        logged_true();
        $this->load->model('connect_model');
        // $this->load->model('logo_model');
        $this->load->library('session');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }
    

	    public function admin_loggin() {
	        $this->load->view('login_view');
	    }

	   

       public function index() {

            $this->form_validation->set_rules('email', 'email','required');
            $this->form_validation->set_rules('password', 'password','required');



            if ($this->form_validation->run() == FALSE)
            {
                $this->load->view('login_view');
            }
            else
            {
                $data_req = array(
                    'mail'                  => $this->input->post('email'),
                    'password'              => $this->input->post('password'),
                );
                $autorisation = $this->connect_model->connect($data_req['mail'], $data_req['password']);
                if(!empty($autorisation)) {
                    $this->session->set_userdata('finaUser',$this->input->post('password'));
                    redirect('../administration/admin_user');
                }
                else {
                    redirect('../connect/admin_loggin');
                }
            }
        }
	}
