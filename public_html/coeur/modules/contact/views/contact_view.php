

<?php $this->load->view('front/header2.php', $var_menu_contact); ?>
<main><br>
  <div class="container" style="background: #fff;">
    <div class="row">
        <center><h1 class="title" style="color:#2E306D;">Contacts</h1></center><br />
      <div style="width: 40%;display: inline-block;">
        <iframe src="https://www.google.com/maps/d/embed?mid=1GDhGS3a8EHNw7L5aulH4aPWEfljG_Ptd&hl=fr" class="embed-responsive-item" width="100%" height="310"></iframe>
      </div>

      <div style="width: 40%;display: inline-block;">
        <h3 class="subtitle">Nous sommes l&agrave; pour vous assister.</h3>
        <form action="contact" method="post">
          <input type="text" name="name" placeholder="Nom Complet*" style="border: 1px solid grey;margin-bottom:6px;width:100%;height:40px;padding-left:10px;border-radius: 2px;" required />
          <input type="email" name="email" placeholder="Votre E-mail*" style="border: 1px solid grey;margin-bottom:6px;width:100%;height:40px;padding-left:10px;border-radius: 2px;" required />
          <input type="tel" name="phone" placeholder="Votre numero de telephone" style="border: 1px solid grey;margin-bottom:6px;width:100%;height:40px;padding-left:10px;border-radius: 2px;" />
          <textarea name="text" id="" rows="8" placeholder="Votre Message*" style="padding-left:10px;height:90px;resize:none;width: 100%;border-radius: 2px;" required></textarea><br />
          <input type="submit" name="submit" class="btn-send">Envoyer message</button>
        </form>
      </div>

    </div>
  </div>
</main>
<?php $this->load->view('front/footer.php'); ?>