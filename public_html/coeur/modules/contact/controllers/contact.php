<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('active');
        $this->load->model('contact_model');
        $this->load->helper('captcha');
        $this->load->library('session');
    }
    
    public function index() {
        $var_user = array();
        $var_user['var_menu_contact'] = "contact";
        $this->load->view('contact_view', $var_user);
    }

}
