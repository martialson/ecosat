<?php

class apropos_model extends CI_Model {

    
      function  get_logo(){
        $this->db->select('*');
        $this->db->from('logo');
        $this->db->order_by('logo_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

       function get_contact(){
        $this->db->select('*');
        $this->db->from('contact_info');
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_historique(){
        $this->db->select('*');
        $this->db->from('historique');
        $this->db->order_by('historique_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

       function  get_objectifs(){
        $this->db->select('*');
        $this->db->from('objectifs');
        $this->db->order_by('objectifs_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_vision(){
        $this->db->select('*');
        $this->db->from('vision');
        $this->db->order_by('vision_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_mission(){
        $this->db->select('*');
        $this->db->from('missions');
        $this->db->order_by('missions_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_dirigeants(){
        $this->db->select('*');
        $this->db->from('notre_equipe');
        $this->db->order_by('notre_equipe_id','desc');
        $this->db->limit(10);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }

      function  get_pourquoi_choisir_finasys(){
        $this->db->select('*');
        $this->db->from('pourquoi_choisir_finasys');
        $this->db->order_by('choisir_finasys_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $results = $query->result();
        return $results;
      }
}
