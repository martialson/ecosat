<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Nos_offres extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('active');
        $this->load->model('apropos_model');
        // $img_logo['contact'] = $this->apropos_model->get_contact();
    }
    
    public function index() {
    	$var_user = array();
        $var_user['var_menu_offres'] = "offres";
        $this->load->view('nos_offres_view', $var_user);
    }


}
