
  <!--========================================================
                            FOOTER
  =========================================================-->
  <footer>
    <div class="parallax" data-url="<?php echo base_url(); ?>assets/images/parallax2.jpg" data-mobile="true" data-speed="0.5 " data-direction="inverted">
      <div class="well4">
        <div class="container center wow fadeInUp" data-wow-delay="0.2s">
          <hr/>
          <h2>ecosat</h2>

          <h3>Souscrire &agrave; un forfait</h3>
          <a class="btn" href='#'>Souscrire!</a>

          <div class="copyright">
            © <span id="copyright-year"></span> Ecosat. All Rights Reserved
            <!-- {%FOOTER_LINK} -->
          </div>
        </div>
      </div>
    </div>
    <div class="well5 center">
      <div class="container">
        <a href='#' style="display: none;"><img src="<?php echo base_url(); ?>assets/images/tm-logo.png" alt="Tempalte Monster"/></a>
      </div>
    </div>

  </footer>
</div>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/script.js"></script>
<script  src="<?php echo base_url(); ?>assets/js/index_menu.js"></script>
<!-- coded by Diversant -->
</body>
</html>