 <!-- Start footer -->
 
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <div class="footer-left">
            <p>Copyright ©2019 <a href="#" class="text-white">Finasys SARL.</a> Tous droits réservés </p>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="footer-right">
            <a href="index.html"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-google-plus"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-pinterest"></i></a>
          </div>
        </div>
      </div>
    </div>


 

 	<script src="<?php echo base_url(); ?>assets/admin/plugins/jquery-1.10.2.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/plugins/pace/pace.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/scripts/siminta.js"></script>
  <!-- End footer -->

</body>

</html>
