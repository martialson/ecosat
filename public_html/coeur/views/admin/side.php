<nav class="navbar-default navbar-static-side" role="navigation">
            <!-- sidebar-collapse -->
            <div class="sidebar-collapse">
                <!-- side-menu -->
                <ul class="nav" id="side-menu">
                    <li>
                        <!-- user image section-->
                        <div class="user-section">
                            <div class="user-section-inner">
                                <img src="<?php echo base_url(); ?>assets/images/apropos/notre_equipe/17122018-112003-13461.jpg" alt="">
                            </div>
                            <div class="user-info">
                                <div> <?php 
                                        echo $this->session->userdata('finaUser');
                                     ?>
                                </strong></div>
                                <div class="user-text-online">
                                    <span class="user-circle-online btn btn-success btn-circle "></span>&nbsp;Online
                                </div>
                            </div>
                        </div>
                        <!--end user image section-->
                    </li>
                    <li class="sidebar-search">
                        <!-- search section-->
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <!--end search section-->
                    </li>
                    <li class="">
                        <a href="<?php echo base_url(); ?>administration/logo"><i class="fa fa-dashboard fa-fw"></i>Logo</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i>Menu principal<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level" style="background: #109EE3;">
                            <li>
                                <a href="<?php echo base_url(); ?>administration/acceuil_list">Accueil</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>administration/apropos_list">A propos</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>administration/nos_services_list">Nos services</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>administration/slides_list"><i class="fa fa-bar-chart-o fa-fw"></i>Slide Accueil</a>
                    </li>
                     <li>
                        <a href="<?php echo base_url(); ?>administration/partenaires"><i class="fa fa-flask fa-fw"></i>Partenaires</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>administration/newsletter"><i class="fa fa-table fa-fw"></i>Newsletter</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>administration/contact"><i class="fa fa-bar-chart-o fa-fw"></i>Contact</a>
                    </li>
                    <li style="display:none;">
                        <a href="#"><i class="fa fa-sitemap fa-fw"></i>Param&egrave;tres<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">Administrateurs<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="#">Ajouter</a>
                                    </li>
                                    <li>
                                        <a href="#">Modifier</a>
                                    </li>
                                    <li>
                                        <a href="#">Suprimer</a>
                                    </li>
                                </ul>
                                <!-- third-level-items -->
                            </li>
                            <li>
                                <a href="#">Passwords<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="#">Modifier</a>
                                    </li>
                                    <li>
                                        <a href="#">bloquer</a>
                                    </li>
                                    <li>
                                        <a href="#">Suprimer</a>
                                    </li>
                                </ul>
                                <!-- third-level-items -->
                            </li>
                            <li>
                                <a href="#">Messages<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="#">G&eacute;rer</a>
                                    </li>
                                    <li>
                                        <a href="#">bloquer</a>
                                    </li>
                                    <li>
                                        <a href="#">vider</a>
                                    </li>
                                </ul>
                                <!-- third-level-items -->
                            </li>
                            <li>
                                <a href="#">Notifications<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="#">G&eacute;rer</a>
                                    </li>
                                    <li>
                                        <a href="#">bloquer</a>
                                    </li>
                                    <li>
                                        <a href="#">vider</a>
                                    </li>
                                </ul>
                                <!-- third-level-items -->
                            </li>
                            <li>
                                <a href="#">T&acirc;ches<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="#">G&eacute;rer</a>
                                    </li>
                                </ul>
                                <!-- third-level-items -->
                            </li>
                        </ul>
                        <!-- second-level-items -->
                    </li>
                </ul>
                <!-- end side-menu -->
            </div>
            <!-- end sidebar-collapse -->
        </nav>
        <!-- end navbar side -->