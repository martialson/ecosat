<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Check admin is login or not
 *
 * 
 * 
 *
 * @access	public
 * @param	string
 * @return	bool
 */
if (!function_exists('logged_true')) {

    function logged_true() {

        $object = &get_instance();

        if ($object->session->userdata('finaUser') == true) {
            redirect('../administration/admin_user', 'refresh');
        }
    }

}

if (!function_exists('logged_false')) {

    function logged_false() {

        $object = &get_instance();

        if ($object->session->userdata('finaUser') == false) {
            redirect('../connect/admin_loggin', 'refresh');
        }
    }

}




