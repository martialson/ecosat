<div class="videos carousel">
    <div class="p20">
        <div class="title-zone">Actualité en<span>Vidéos</span> </div>

        <div id="owl-slider-video">
            <?php  foreach($videos as $row){ ?>
                <div class="item">
                    <div class="service-item video">
                        <div class="relative">
                            <i class="fa fa-video-camera"></i>
                            <a href="<?php echo base_url()?>mediatheque/lire_video/<?php echo $row->video_slug; ?>"><img data-src="http://i1.ytimg.com/vi/<?php echo youtube_embed($row->video_lien);?>/mqdefault.jpg" alt="" title="" width="190" height="107"></a>
                        </div>
                        <div class="p10-20">
                            <h6><?php  echo substr($row->video_titre,0,30); ?> ...</h6>

                            <div class="date"><a href="#"> <i class="fa fa-clock-o"></i><?php echo date('d/m/Y', strtotime($row->media_date));  ?></a></div>

                        </div>
                    </div>
                </div>
            <?php  } ?>
        </div>
    </div>
</div>