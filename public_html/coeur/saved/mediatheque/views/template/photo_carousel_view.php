<!-- <div class="photo carousel">
    <div class="p20">
        <div class="title-zone">Actualité en<span>images</span> </div>

        <div id="owl-slider-photo">
            <?php if(!empty($photos)){foreach($photos as $row){?>
                <div class="item">
                    <div class="service-item photo">
                        <div class="relative">
                            <i class="fa fa-picture-o"></i>
                            <a href="#"><img src="<?php echo base_url() ?>uploads/medias/<?php echo $row->media_image;?>" alt="" title=""></a>
                        </div>
                        <div class="p10-20">
                            <h6><?php echo $row->media_titre;?></h6> <div class="date"><a href="#"> <i class="fa fa-clock-o"></i> 01/04/2016</a></div>

                        </div>
                    </div>
                </div>
            <?php }}   ?>

        </div>
    </div>
</div> -->
<div class="photo carousel">
    <div class="p20">
        <div class="title-zone">Actualité en<span>images</span> </div>

        <div id="owl-slider-photo">
            <div class="item">
                <div class="service-item photo">
                    <div class="relative">
                        <i class="fa fa-picture-o"></i>
                        <a href="#"><img src="<?php echo base_url() ?>assets/images/proclamation_bac.jpg" alt="" title="" width="190px" height="126px"></a>
                    </div>
                    <div class="p10-20">
                        <h6>BAC 2016: PROCLAMATION DES RESULTATS</h6>

                        <div class="date"><a href="#"> <i class="fa fa-clock-o"></i> 30/08/2016</a></div>

                    </div>
                </div>
            </div>

            <div class="item">
                <div class="service-item photo">
                    <div class="relative">
                        <i class="fa fa-picture-o"></i>
                        <a href="#"><img src="<?php echo base_url() ?>assets/images/Inscription-ligne-Resultats-CEPE-0013.jpg" alt="" title="" width="190px" height="126px"></a>
                    </div>
                    <div class="p10-20">
                        <h6>Les inscriptions en ligne 2016-2017 sont ouvertes du 30 Juin au 10 Septembre 2016</h6>
                        <div class="date"><a href="#"> <i class="fa fa-clock-o"></i> 30/08/2016</a></div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="service-item photo">
                    <div class="relative">
                        <i class="fa fa-picture-o"></i>
                        <a href="#"><img src="<?php echo base_url() ?>assets/images/Afrique_Anne.jpg" alt="" title="" width="190px" height="126px"></a>
                    </div>
                    <div class="p10-20">
                        <h6>Le ministère de l'éducation nationale milite en faveur de la scolarisation massive des femmes</h6>
                        <div class="date"><a href="#"> <i class="fa fa-clock-o"></i> 30/08/2016</a></div>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="service-item photo">
                    <div class="relative">
                        <i class="fa fa-picture-o"></i>
                        <a href="#"><img src="<?php echo base_url() ?>assets/images/women_right.jpg" alt="" title="" width="190px" height="126px"></a>
                    </div>
                    <div class="p10-20">
                        <h6>Respect des droits des femmes: le droit à la scolarisation de la femme doit être observé partout dans le monde entier</h6>
                        <div class="date"><a href="#"> <i class="fa fa-clock-o"></i> 30/08/2016</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
