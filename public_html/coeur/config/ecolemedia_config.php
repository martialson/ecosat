<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| configuration d'Ecolemedia by Hermann N'djomon  31/07/2017

|--------------------------------------------------------------------------
|

*/



global $MONGO_DATABASE;

global $MONGO_OLD_DATABASE;
global $PORT;
global $MONGO_USER;
global $MONGO_PASSWORD;
global $MONGO_HOST;

$config['new_database'] = $MONGO_DATABASE;
$config['annee_academique'] = "2018-2019";
$config['old_database'] = $MONGO_OLD_DATABASE;

$config['port'] = $PORT;
$config['dns'] = "mongodb://$MONGO_HOST:$PORT/$MONGO_DATABASE";
$config['option'] = array('username'=>$MONGO_USER, 'password'=>$MONGO_PASSWORD);



